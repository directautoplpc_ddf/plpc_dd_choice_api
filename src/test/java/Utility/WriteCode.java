package Utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class WriteCode
{
	public static void writetofile(String  filecontent, String scenarioname) throws IOException
	{
		
		
		String fos = "INT01.csv";
	 
		BufferedWriter bw = new BufferedWriter(new FileWriter(fos, true));
		bw.write(scenarioname);
		bw.newLine();
		
		bw.write(filecontent);
		bw.newLine();
		bw.close();
}
	public static void writetofiles(String  filecontents) throws IOException
	{
		
		//File fout = new File("out.txt");
		String foss = "INT01.csv";
		BufferedWriter bws = new BufferedWriter(new FileWriter(foss, true));
		bws.write(filecontents);
		bws.newLine();
		bws.close();
}
	public static String changeformat(String data)
	{
		if(data.length()==0)
		{
			return data;
		}
		String datanew = data;
		String day =datanew.split("/")[0];
//		int Day =Integer.parseInt(day);
		String month=datanew.split("/")[1];
//		int Month =Integer.parseInt(day);
		String year=datanew.split("/")[2];
//		int Year =Integer.parseInt(day);
		if(day.length()<2)
    	{
    	day='0'+day;
    	}
    	if(month.length()<2)
    	{
    	month='0'+month;
    	}
		return year+'-'+day+'-'+month;
	}
}