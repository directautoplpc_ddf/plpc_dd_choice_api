package Utility;

import com.github.javafaker.Faker;

public class NameFaker
{
	public static String methodname()
	{
		Faker faker = new Faker();

		String firstName = faker.name().firstName(); 
		return firstName;
	}
	
	public static String methodnametwo()
	{
		Faker fakers = new Faker();
		String lastName = fakers.name().lastName(); 
		return lastName;
	}
	
	public static String methodnamedrivertwo()
	{
		Faker fakerdrivertwo = new Faker();

		String firstName = fakerdrivertwo.name().firstName(); 
		return firstName;
	}
	
	public static String methodnametwodrivertwo()
	{
		Faker fakersdrivertwo = new Faker();
		String lastName = fakersdrivertwo.name().lastName(); 
		return lastName;
	}
	
	public static String methodnamedriverthree()
	{
		Faker fakerdriverthree = new Faker();

		String firstName = fakerdriverthree.name().firstName(); 
		return firstName;
	}
	
	public static String methodnametwodriverthree()
	{
		Faker fakersdriverthree = new Faker();
		String lastName = fakersdriverthree.name().lastName(); 
		return lastName;
	}
	public static String methodnamedriverfour()
	{
		Faker fakerdriverfour = new Faker();

		String firstName = fakerdriverfour.name().firstName(); 
		return firstName;
	}
	
	public static String methodnametwodriverfour()
	{
		Faker fakersdriverfour = new Faker();
		String lastName = fakersdriverfour.name().lastName(); 
		return lastName;
	}
}


