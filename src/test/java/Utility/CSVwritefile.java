package Utility;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CSVwritefile
 {
	public static int i = 10;
	public static int j = i;
	
	
public static void updateCSVfile(String replace) throws IOException 
      {
	File inputFile = new File("C://AmfamScript//Test//AggregatorSecond//Submission.csv");

	// Read existing file 
	CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
	
	List<String[]> csvBody = reader.readAll();
	// get CSV row column  and replace with by using row and column
	
		csvBody.get(i)[1] = replace;
		//new
		i++;

	
	//csvBody.get(1)[1] = replace;
	//List<String[]> csvBody = reader.readAll();
	// get CSV row column  and replace with by using row and column
	
	reader.close();

	// Write to CSV file which is open
	CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
	writer.writeAll(csvBody);
	writer.flush();
	writer.close();
	}

public static Map<String, Integer> getDate() {
	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month = Calendar.getInstance().get(Calendar.MONTH);
	int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

	Map<String, Integer> todayDate = new HashMap<String, Integer>();
	todayDate.put("year", year);
	todayDate.put("month", month);
	todayDate.put("day", day);

	return todayDate;
}

public static String zip(int number)
{

 String formatted = String.format("%05d", number);
 return formatted;
}
public static String zip2(int number)
{

 String formatted = String.format("%02d", number);
 return formatted;
}

public static String CensusBlockMD(int number)
{

 String formatted = String.format("%03d", number);
 return formatted;
}

public static String CensusTrack(int number)
{

 String formatted = String.format("%06d", number);
 return formatted;
}

public static String FIPSCountyNum(int number)
{

 String formatted = String.format("%03d", number);
 return formatted;
}

public static String BlockGroupKey(int number)
{

 String formatted = String.format("%12d", number);
 return formatted;
}



public static void updateCSVfilesubmission(String replaces) throws IOException 
{
File inputFiles = new File("C://AmfamScript//Test//AggregatorSecond//Submission.csv");

// Read existing file 
CSVReader readers = new CSVReader(new FileReader(inputFiles), ',');

List<String[]> csvBodys = readers.readAll();
// get CSV row column  and replace with by using row and column

	csvBodys.get(j)[2] = replaces;
//remove old
j++;
//csvBody.get(1)[1] = replace;
//List<String[]> csvBody = reader.readAll();
// get CSV row column  and replace with by using row and column

readers.close();

// Write to CSV file which is open
CSVWriter writers = new CSVWriter(new FileWriter(inputFiles), ',');

writers.writeAll(csvBodys);
writers.flush();
writers.close();
}
      }
 
