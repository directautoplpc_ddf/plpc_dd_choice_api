@new
Feature: Two Drivers and Two Vehicles 

Background: Testing 
	* configure headers = {Content-Type: 'application/xml'}
	* configure ssl = true
	* configure ssl = 'TLSv1.2'
	
	* def writerfirst = Java.type('Utility.NameFaker') 
	* def firstname = writerfirst.methodname( ) 
	* def writerlast = Java.type('Utility.NameFaker') 
	* def lastname = writerlast.methodnametwo( ) 
	* def writerOperatorfirst = Java.type('Utility.NameFaker') 
	* def Operatorfirstname = writerOperatorfirst.methodname( ) 
	* def writerOperatorlast = Java.type('Utility.NameFaker') 
	* def Operatorlastname = writerOperatorlast.methodnametwo( ) 
	

# 	CTP QA
#   * url 'https://plpc-qa.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11' 
 
#   CTP INT
#   * url 'http://igwplco001:8080/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
 
#  INT01
   * url 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11' 
 
#  PINTD
#  * url 'http://dgwplco201:8080/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11' 
   
Scenario: Passing values from testdata 
	
	* xml req = read('classpath:resources/common/BaseXml/2V2D_Operators/State Rollout_2V2D_AR.xml')
	* def scenario = TestScenario
	
	
	* def LOBB = LOB == 'Null' ? karate.remove ('req','//LOB') : karate.set ('req','//LOB',LOB)
	* def contract = ContractState == 'Null' ? karate.remove ('req','//ContractState') : karate.set ('req','//ContractState',ContractState)
	
	#Line coverages
	
	* def coverage1 = PABodilyInjuryCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[1]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[1]/Terms/Entry/SelectedValue',PABodilyInjuryCov_af)
	* def coverage2 = PAPropDamageCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[2]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[2]/Terms/Entry/SelectedValue',PAPropDamageCov_af)
	* def coverage3 = PAMedExpCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[3]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[3]/Terms/Entry/SelectedValue',PAMedExpCov_af)
	* def coverage4 = PAUMCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[4]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[4]/Terms/Entry/SelectedValue',PAUMCov_af)
  * def coverage5 = PAUMPDLineLimit == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[5]/Terms/Entry[1]/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[5]/Terms/Entry[1]/SelectedValue',PAUMPDLineLimit)
  * def coverage6 = PAUMPDLineDed == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[5]/Terms/Entry[2]/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[5]/Terms/Entry[2]/SelectedValue',PAUMPDLineDed)
  * def coverage7 = PAUIMCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[6]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[6]/Terms/Entry/SelectedValue',PAUIMCov_af)
  * def coverage8 = PAPipAccDeath_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[7]/Terms/Entry[1]/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[7]/Terms/Entry[1]/SelectedValue',PAPipAccDeath_af)
  * def coverage9 = PAPipIncDisability_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[7]/Terms/Entry[2]/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[7]/Terms/Entry[2]/SelectedValue',PAPipIncDisability_af)
  * def coverage10 = PAPipMedical_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[7]/Terms/Entry[3]/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[7]/Terms/Entry[3]/SelectedValue',PAPipMedical_af)
   
   #Effective date
   * def functions = call read('classpath:Utility/Utility.js')
	 * def EffectiveDate = functions.getCurrentDate( )
   * def date = EffectiveDate == 'Null' ? karate.remove ('req','//Product/EffectiveDate') : karate.set ('req','//Product/EffectiveDate',EffectiveDate+'T01:00:00-05:00')
    
    #PolicyAddress
    
    * def carrierRoute = CarrierRoute == 'Null' ? karate.remove ('req','//Product/PolicyAddress/CarrierRoute') : karate.set ('req','//Product/PolicyAddress/CarrierRoute',CarrierRoute)
     * def censusBlockGroup = CensusBlockGroup == 'Null' ? karate.remove ('req','//Product/PolicyAddress/CensusBlockGroup') : karate.set ('req','//Product/PolicyAddress/CensusBlockGroup',CensusBlockGroup)
     * def BlockMD = CensusBlockMD
     * string BlockMDD = BlockMD
     * def Census_BlockMD = Java.type('Utility.CSVwritefile')
     * def CensusBlockMD = Census_BlockMD.CensusBlockMD(BlockMDD)
     * def censusBlockMD = CensusBlockMD == 'Null' ? karate.remove ('req','//Product/PolicyAddress/CensusBlockMD') : karate.set ('req','//Product/PolicyAddress/CensusBlockMD',CensusBlockMD)
     
     
    * def Track = CensusTrack
    * string CensusTrackk = Track
    * def Census_Track = Java.type('Utility.CSVwritefile')
    * def CensusTrack = Census_Track.CensusTrack(CensusTrackk)
    * def censusTrack = CensusTrack == 'Null' ? karate.remove ('req','//Product/PolicyAddress/CensusTrack') : karate.set ('req','//Product/PolicyAddress/CensusTrack',CensusTrack)
    
    * def city = City == 'Null' ? karate.remove ('req','//Product/PolicyAddress/City') : karate.set ('req','//Product/PolicyAddress/City',City)
    * def country = Country == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Country') : karate.set ('req','//Product/PolicyAddress/Country',Country)
    #* def Policycounty = County == 'Null' ? karate.remove ('req','//Product/PolicyAddress/County') : karate.set ('req','//Product/PolicyAddress/County',County)
    * def FIPS = FIPSCountyNum
    * string FIPSCounty = FIPS
    * def FIPS_CountyNum = Java.type('Utility.CSVwritefile')
    * def FIPSCountyNum = FIPS_CountyNum.FIPSCountyNum(FIPSCounty)
    * def fIPSCountyNum = FIPSCountyNum == 'Null' ? karate.remove ('req','//Product/PolicyAddress/FIPSCountyNum') : karate.set ('req','//Product/PolicyAddress/FIPSCountyNum',FIPSCountyNum)
   
    * def fIPSStateNum = FIPSStateNum == 'Null' ? karate.remove ('req','//Product/PolicyAddress/FIPSStateNum') : karate.set ('req','//Product/PolicyAddress/FIPSStateNum',FIPSStateNum)
    
    * def Lat = Latitude == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Latitude') : karate.set ('req','//Product/PolicyAddress/Latitude',Latitude)
    * def Line = Line1 == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Line1') : karate.set ('req','//Product/PolicyAddress/Line1',Line1)
    * def Long = Longitude == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Longitude') : karate.set ('req','//Product/PolicyAddress/Longitude',Longitude)
    * def ShortCityy = ShortCity == 'Null' ? karate.remove ('req','//Product/PolicyAddress/ShortCity') : karate.set ('req','//Product/PolicyAddress/ShortCity',ShortCity) 
    * def state = State == 'Null' ? karate.remove ('req','//Product/PolicyAddress/State') : karate.set ('req','//Product/PolicyAddress/State',State)
     
    * def zipvalue1 = ZipCode
    * string zip1 = zipvalue1
    * def postal1 = Java.type('Utility.CSVwritefile')
    * def Zip = postal1.zip(zip1)
    * def Zipp = Zip == 'Null' ? karate.remove ('req','//Product/PolicyAddress/ZipCode') : karate.set ('req','//Product/PolicyAddress/ZipCode',Zip)
    
    * def zipPlus = ZipPlus == 'Null' ? karate.remove ('req','//Product/PolicyAddress/ZipPlus4') : karate.set ('req','//Product/PolicyAddress/ZipPlus4',ZipPlus)
    
    #Affiliation
    
    * def Tier = MemberTier == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Affiliations/Entry/MemberTier') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Affiliations/Entry/MemberTier',MemberTier)
   
    #PNI Address
    
    * def PNICarrierRoutee = PNICarrierRoute == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CarrierRoute') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CarrierRoute',PNICarrierRoute)
    * def PNICensusBlockGroupp = PNICensusBlockGroup == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CensusBlockGroup') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CensusBlockGroup',PNICensusBlockGroup)
    * def PNICensusBlockMDD = CensusBlockMD == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CensusBlockMD') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CensusBlockMD',CensusBlockMD)
    * def PNICensusTrackk = CensusTrack == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CensusTrack') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/CensusTrack',CensusTrack)
    * def PNICityy = PNICity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/City',PNICity)
    #* def PNICounty = PNICounty == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/County') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/County',PNICounty)
    * def PNIFIPSCountyNumm = FIPSCountyNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/FIPSCountyNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/FIPSCountyNum',FIPSCountyNum)
    * def PNIFIPSStateNumm = PNIFIPSStateNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/FIPSStateNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/FIPSStateNum',PNIFIPSStateNum)
    * def PNILat1 = PNILat == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Latitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Latitude',PNILat)
    * def PNILine11 = PNILine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Line1',PNILine1)
    * def PNILong1 = PNILong == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Longitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Longitude',PNILong)
    * def PNIShortCityy = PNIShortCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ShortCity') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ShortCity',PNIShortCity)
    * def PNIStatee = PNIState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/State',PNIState)
    * def PNIZipcodee = Zip == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ZipCode',Zip)
    * def PNIZipPluss = PNIZipPlus == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ZipPlus4') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ZipPlus4',PNIZipPlus)
 
    #PNI Details
    
    * def birthDate = BirthDate == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/BirthDate') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/BirthDate',BirthDate)
    * def firstName = FirstName == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/FirstName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/FirstName',firstname)
    * def lastName = LastName == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LastName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LastName',lastname)
    * def License = LicenseNumber == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseNumber') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseNumber',LicenseNumber)
    * def licenseState = LicenseState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseState') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseState',LicenseState)
   
    #Operator Address
    
    * def OperatorCarrierRoutee = OperatorCarrierRoute == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CarrierRoute') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CarrierRoute',OperatorCarrierRoute)
    * def OperatorCensusBlockGroupp = OperatorCensusBlockGroup == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CensusBlockGroup') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CensusBlockGroup',OperatorCensusBlockGroup)
    * def OperatorCensusBlockMDD = CensusBlockMD == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CensusBlockMD') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CensusBlockMD',CensusBlockMD)
    * def OperatorCensusTrackk = CensusTrack == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CensusTrack') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/CensusTrack',CensusTrack)
    * def OperatorCityy = OperatorCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/City',OperatorCity)
   # * def OperatorCounty = OperatorCounty == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/County') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/County',OperatorCounty)
    * def OperatorFIPSCountyNumm = FIPSCountyNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/FIPSCountyNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/FIPSCountyNum',FIPSCountyNum)
    * def OperatorFIPSStateNumm = OperatorFIPSStateNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/FIPSStateNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/FIPSStateNum',OperatorFIPSStateNum)
    * def OperatorLat1 = OperatorLat == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Latitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Latitude',OperatorLat)
    * def OperatorLine11 = OperatorLine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Line1',OperatorLine1)
    * def OperatorLong1 = OperatorLong == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Longitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Longitude',OperatorLong)
    * def OperatorShortCityy = OperatorShortCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ShortCity') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ShortCity',OperatorShortCity)
    * def OperatorStatee = OperatorState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/State',OperatorState)
    * def OperatorZipcodee = Zip == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ZipCode',Zip)
    * def OperatorZipPluss = OperatorZipPlus == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ZipPlus4') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ZipPlus4',OperatorZipPlus)
    
    #Operator Details
    
    * def birthDate1 = BirthDate1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/BirthDate') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/BirthDate',BirthDate1)
    * def firstName1 = FirstName1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/FirstName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/FirstName',Operatorfirstname)
    * def lastName1 = LastName1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LastName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LastName',Operatorlastname)
    * def licenseNumber1 = LicenseNumber1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseNumber') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseNumber',LicenseNumber1)
    * def licenseState1 = LicenseState1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseState') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseState',LicenseState1)
   
   #Vehicle1 coverage 
   
    * def Vehicle1PACollisionCov = PACollisionCov == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Coverages/Entry[1]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Coverages/Entry[1]/Terms/Entry/SelectedValue',PACollisionCov)
    * def Vehicle1PAComprehensiveCov = PAComprehensiveCov == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Coverages/Entry[2]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Coverages/Entry[2]/Terms/Entry/SelectedValue',PAComprehensiveCov)
   
   #Vehicle1 Garaged address
   
  # * def Veh1BlockGroupKey = Vehicle1BlockGroupKey == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/BlockGroupKey') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/BlockGroupKey',Vehicle1BlockGroupKey)
  # * def Veh1CarrierRoute = Vehicle1CarrierRoute == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CarrierRoute') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CarrierRoute',Vehicle1CarrierRoute)
   * def Veh1CensusBlockGroup = Vehicle1CensusBlockGroup == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CensusBlockGroup') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CensusBlockGroup',Vehicle1CensusBlockGroup)
   * def Veh1CensusBlockMD = CensusBlockMD == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CensusBlockMD') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CensusBlockMD',CensusBlockMD)
  # * def Veh1CensusTrack = Vehicle1CensusTrack == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CensusTrack') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/CensusTrack',Vehicle1CensusTrack)
   * def Veh1GaragedCity = Vehicle1GaragedCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/City',Vehicle1GaragedCity)
   * def Veh1GaragedCounty = Vehicle1GaragedCounty == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/County') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/County',Vehicle1GaragedCounty)
    * def Veh1FIPSCountyNum = FIPSCountyNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/FIPSCountyNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/FIPSCountyNum',FIPSCountyNum)
   * def Veh1FIPSStateNum = Vehicle1FIPSStateNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/FIPSStateNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/FIPSStateNum',Vehicle1FIPSStateNum)
   * def Veh1GaragedLatitude = Vehicle1GaragedLatitude == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Latitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Latitude',Vehicle1GaragedLatitude)
   * def Veh1GaragedLine1 = Vehicle1GaragedLine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Line1',Vehicle1GaragedLine1)
   * def Veh1GaragedLongitude = Vehicle1GaragedLongitude == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Longitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Longitude',Vehicle1GaragedLongitude)
   * def Veh1ShortCity = Vehicle1ShortCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ShortCity') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ShortCity',Vehicle1ShortCity)
   * def Veh1GaragedState = Vehicle1GaragedState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/State',Vehicle1GaragedState)
   * def Veh1GaragedZipCode = Zip == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ZipCode',Zip)
  * def Veh1ZipPlus4 = Vehicle1ZipPlus4 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ZipPlus4') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ZipPlus4',Vehicle1ZipPlus4)
  
   #Vehicle1 details  
      
    * def Vehicle1Model = Model == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Model') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Model',Model)
    * def Vehicle1Vin = Vin == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Vin') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Vin',Vin)
    * def Vehicle1Year = Year == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Year') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Year',Year)
  
   #Vehicle2 coverage 
     
    * def Vehicle2PACollisionCov = PACollisionCov == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Coverages/Entry[1]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Coverages/Entry[1]/Terms/Entry/SelectedValue',PACollisionCov)
    * def Vehicle2PAComprehensiveCov = PAComprehensiveCov == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Coverages/Entry[2]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Coverages/Entry[2]/Terms/Entry/SelectedValue',PAComprehensiveCov)
    
   #Vehicle2 Garaged address
   
  # * def Veh2BlockGroupKey = Vehicle2BlockGroupKey == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/BlockGroupKey') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/BlockGroupKey',Vehicle2BlockGroupKey)
  # * def Veh2CarrierRoute = Vehicle2CarrierRoute == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CarrierRoute') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CarrierRoute',Vehicle2CarrierRoute)
   * def Veh2CensusBlockGroup = Vehicle2CensusBlockGroup == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CensusBlockGroup') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CensusBlockGroup',Vehicle2CensusBlockGroup)
   * def Veh2CensusBlockMD = CensusBlockMD == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CensusBlockMD') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CensusBlockMD',CensusBlockMD)
  # * def Veh2CensusTrack = Vehicle2CensusTrack == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CensusTrack') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/CensusTrack',Vehicle2CensusTrack)
   * def Veh2GaragedCity = Vehicle2GaragedCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/City',Vehicle2GaragedCity)
   * def Veh2GaragedCounty = Vehicle2GaragedCounty == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/County') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/County',Vehicle2GaragedCounty)
   * def Veh2FIPSCountyNum = FIPSCountyNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/FIPSCountyNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/FIPSCountyNum',FIPSCountyNum)
   * def Veh2FIPSStateNum = Vehicle2FIPSStateNum == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/FIPSStateNum') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/FIPSStateNum',Vehicle2FIPSStateNum)
   * def Veh2GaragedLatitude = Vehicle2GaragedLatitude == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/Latitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/Latitude',Vehicle2GaragedLatitude)
   * def Veh2GaragedLine1 = Vehicle2GaragedLine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/Line1',Vehicle2GaragedLine1)
   * def Veh2GaragedLongitude = Vehicle2GaragedLongitude == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/Longitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/Longitude',Vehicle2GaragedLongitude)
   * def Veh2ShortCity = Vehicle2ShortCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/ShortCity') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/ShortCity',Vehicle2ShortCity)
   * def Veh2GaragedState = Vehicle2GaragedState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/State',Vehicle2GaragedState)
   * def Veh2GaragedZipCode = Zip == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/ZipCode',Zip)
   * def Veh2ZipPlus4 = Vehicle2ZipPlus4 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/ZipPlus4') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/GaragedLocation/LocationAddress/ZipPlus4',Vehicle2ZipPlus4)
   
   #Vehicle2 details 
     
    * def Vehicle2Model = Model1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Model') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Model',Model1)
    * def Vehicle2Vin = Vin1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Vin') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Vin',Vin1)
    * def Vehicle2Year = Year1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Year') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[2]/Year',Year1)
    
    #PrefillID
   
    * def PrefillID = PrefillReportID == 'Null' ? karate.remove ('req','//Product/PrefillReportID') : karate.set ('req','//Product/PrefillReportID',PrefillReportID)
    
    #Primary name insured
    
    * def PNICarrierRoutee = PNICarrierRoute == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CarrierRoute') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CarrierRoute',PNICarrierRoute)
    * def PNICensusBlockGroupp = PNICensusBlockGroup == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CensusBlockGroup') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CensusBlockGroup',PNICensusBlockGroup)
    * def PNICensusBlockMDD = CensusBlockMD == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CensusBlockMD') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CensusBlockMD',CensusBlockMD)
    * def PNICensusTrackk = CensusTrack == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CensusTrack') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/CensusTrack',CensusTrack)
    * def PNICityy = PNICity == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/City') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/City',PNICity)
   # * def PNICountyy = PNICounty1 == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/County') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/County',PNICounty1)
    
    * def PNIFIPSCountyNumm = FIPSCountyNum == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/FIPSCountyNum') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/FIPSCountyNum',FIPSCountyNum)
    * def PNIFIPSStateNumm = PNIFIPSStateNum == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/FIPSStateNum') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/FIPSStateNum',PNIFIPSStateNum)
    * def PNILat1 = PNILat == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Latitude') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Latitude',PNILat)
    * def PNILine11 = PNILine1 == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Line1') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Line1',PNILine1)
    * def PNILong1 = PNILong == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Longitude') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Longitude',PNILong)
    * def PNIShortCityy = PNIShortCity == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ShortCity') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ShortCity',PNIShortCity)
    * def PNIStatee = PNIState == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/State') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/State',PNIState)
    * def PNIZipcodee = Zip == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ZipCode') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ZipCode',Zip)
    * def PNIZipPluss = PNIZipPlus == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ZipPlus4') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ZipPlus4',PNIZipPlus)
 
    * def birthDate = BirthDate == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/BirthDate') : karate.set ('req','//Product/PrimaryNamedInsured/Person/BirthDate',BirthDate)
    * def firstName = FirstName == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/FirstName') : karate.set ('req','//Product/PrimaryNamedInsured/Person/FirstName',firstname)
    * def lastName = LastName == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/LastName') : karate.set ('req','//Product/PrimaryNamedInsured/Person/LastName',lastname)
    * def License = LicenseNumber == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/LicenseNumber') : karate.set ('req','//Product/PrimaryNamedInsured/Person/LicenseNumber',LicenseNumber)
    * def licenseState = LicenseState == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/LicenseState') : karate.set ('req','//Product/PrimaryNamedInsured/Person/LicenseState',LicenseState)
   
    #PriorPolicies
    
    * def ContractState = ContractState1 == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/ContractState') : karate.set ('req','//Product/PriorPolicies/Entry/ContractState',ContractState1)
    
    * def PriorPolicyFirstname = PolicyFirstName == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/PolicyFirstName') : karate.set ('req','//Product/PriorPolicies/Entry/PolicyFirstName',firstname)
    * def PriorPolicyFullName = PolicyLastName == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/PolicyFullName') : karate.set ('req','//Product/PriorPolicies/Entry/PolicyFullName',firstname+lastname)
    * def PriorPolicyLastName = PolicyLastName == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/PolicyLastName') : karate.set ('req','//Product/PriorPolicies/Entry/PolicyLastName',lastname)
    
    Given request req 
    When method POST 
    And print response 
    And print scenario 
  
	#Checking whether getting submission id in GB Response
	
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200
	
	# output response
	And def submission = karate.jsonPath(response,"$..['pogo:SubmissionID']")[0]
	And print submission 
	And match submission != null 
  
  #Exporting submission id
	And string sub = submission
	And def demowrite = Java.type('Utility.WriteCode') 
	And def Addres = demowrite.writetofile(sub,scenario)

 #Premium Validation
	And def MonthlyPremium = karate.jsonPath(response,"$..['pogo:MonthlyPremium']")[0]
	And print 'MonthlyPremium', MonthlyPremium    
	And match MonthlyPremium != null
	And def TotalPremium = karate.jsonPath(response,"$..['pogo:TotalPremium']")[0]
	And print 'TotalPremium', TotalPremium
	And match TotalPremium != null
	
	# GetBindable Premium Verify with Recalc
	And xml requ = read ('classpath:resources/common/BaseXml/RecalcPremium.xml') 
	And karate.remove ('requ','/soapenv:Envelope/soapenv:Body/pcs:recalc/pcs:aRequest/ent:SubmissionID') 
	And karate.set ('requ','/soapenv:Envelope/soapenv:Body/pcs:recalc/pcs:aRequest/ent:SubmissionID',submission) 
	And print requ 
	Given request requ 
	
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
	

	Then retry until responseStatus == 200
	And print 'response: ', response 
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	# Then assert responseStatus == 200
	
	#output response
	And def submission_GetBindableRecalc = karate.jsonPath(response,"$..['pogo:SubmissionID']")[0] 
	And print submission_GetBindableRecalc
	And match submission_GetBindableRecalc != null 

	
	#Exporting submission id
	And string sub = submission_GetBindableRecalc 
	And def demowrite = Java.type('Utility.WriteCode') 
	And def Addres = demowrite.writetofile(sub,scenario)
	
 	#RecalcMemberAdd
			  
  And xml reqsn = read ('classpath:resources/common/BaseXml/RecalcMember.xml') 
  And karate.remove ('reqsn','/soap:Envelope/soap:Body/ns2:recalc/ns2:aRequest/SubmissionID') 
  And print reqsn
  And karate.set ('reqsn','/soap:Envelope/soap:Body/ns2:recalc/ns2:aRequest/SubmissionID',submission)
  And print reqsn
	Given request reqsn 
	 
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	Then retry until responseStatus == 200
	And print 'response: ', response 
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200
	
  # GetForms_to be signed Document
	  
  And xml reqsn = read ('classpath:resources/common/BaseXml/getSignatureForms.xml') 
  And karate.remove ('reqsn','/soapenv:Envelope/soapenv:Body/pcs:getSignatureForms/pcs:signDocumentRequestType/ent:SubmissionID') 
  And print reqsn
  And karate.set ('reqsn','/soapenv:Envelope/soapenv:Body/pcs:getSignatureForms/pcs:signDocumentRequestType/ent:SubmissionID',submission)
  And print reqsn
  Given request reqsn 
	 
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	Then status 200 
  And print 'response: ', response 
  # check if the response status is 200
	And def uploadStatusCode = responseStatus 
 Then assert responseStatus == 200
	 
  # Getdocsigned Document
	  
  And xml reqdcn = read ('classpath:resources/common/BaseXml/getdocforonlinesign.xml') 
  And karate.remove ('reqdcn','/soapenv:Envelope/soapenv:Body/pcs:getDocumentsForOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID') 
  And print reqdcn
  And karate.set ('reqdcn','/soapenv:Envelope/soapenv:Body/pcs:getDocumentsForOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID',submission)
  And print reqdcn
	Given request reqdcn 
	
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	 Then status 200 
   And print 'response: ', response 
	 # check if the response status is 200
	 And def uploadStatusCode = responseStatus 
	 Then assert responseStatus == 200
	 
	 # Signed Document online
	And xml reqso = read ('classpath:resources/common/BaseXml/signDocUOnline.xml') 
	And karate.remove ('reqso','/soapenv:Envelope/soapenv:Body/pcs:signDocumentsUsingOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID') 
	And karate.set ('reqso','/soapenv:Envelope/soapenv:Body/pcs:signDocumentsUsingOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID',submission) 
	And print reqso 
	Given request reqso 

  #Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	
	Then status 200 
	And print 'response: ', response 
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200 
	 
	Then status 200 
	And print 'response: ', response 
	# check if the response status is 200
  And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200
	  
 # Bind submission
  And xml reqs = read ('classpath:resources/common/BaseXml/submission.xml') 
  And karate.remove ('reqs','/soapenv:Envelope/soapenv:Body/pcs:bindSubmission/pcs:submissionID') 
  And karate.set ('reqs','/soapenv:Envelope/soapenv:Body/pcs:bindSubmission/pcs:submissionID',submission) 
  And print reqs 
  Given request reqs 
  #Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
              
  # Then status 200 
   Then retry until responseStatus == 200
   And print 'response: ', response 
  # check if the response status is 200
   And def uploadStatusCode = responseStatus 
   Then assert responseStatus == 200
	
	# Retrieve submission
              * configure headers = {Content-Type: 'text/xml; charset=utf-8'}
              * configure logPrettyResponse = true
              * configure ssl = true
              * configure ssl = 'TLSv1.2'
              * configure readTimeout = 3000000

              * configure charset = null
              * configure headers = {Cookie : null}
              
    #Environment
              
    * url 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionDataService/soap11'
              
  
  And xml reqs = read ('classpath:resources/common/BaseXml/RetrieveSubmission.xml') 
  And karate.remove ('reqs','/soapenv:Envelope/soapenv:Body/pcs:retrieveSubmission/pcs:aRequest/ent:SubmissionID') 
  And karate.set ('reqs','/soapenv:Envelope/soapenv:Body/pcs:retrieveSubmission/pcs:aRequest/ent:SubmissionID',submission) 
  And print reqs 

  Given request reqs 
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionDataService/soap11'
  Then status 200
 # Then retry until status == 200
  And print 'response: ', response
  
 #Validation in Policy retrive
 
  #1. Validating Bodily injury
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag1
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD1
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
  #2. Validating Medical expense     
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag2
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD2
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
   #3. Validating Property damage      
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag3
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD3
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
    #4. Validating Comprehensive     
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag4
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD4
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
    #5. Validating Collision     
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag5
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD5
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
    #6. Validating Accident Free discount
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag6
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:Selected']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD6
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
	