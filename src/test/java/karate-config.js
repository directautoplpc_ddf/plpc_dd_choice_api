function fn() {    
	  var env = karate.env; // get system property 'karate.env'
	  
	  //Configure report logs to showcase only steps with keywords
	  karate.configure('report', { showLog: true, showAllSteps: true })
	  //Configure all response to be formatted in Pretty Print
	  karate.configure('logPrettyResponse', true)
	  
	  karate.log('karate.env system property was:', env)
	  var config = {
    env: env,
	  };
	  return config;
	}

/*function() {
	  var env = karate.env; // get java system property 'karate.env'
	 
	  karate.log('karate.env selected environment was:', env);
	  karate.configure("ssl", true)
	 
	  if (!env) {
	    env = 'dev'; //env can be anything: dev, qa, staging, etc.
	  }
	 
	  var config = {
	    env: env,
	 
	    AM_USERNAME: 'devuser',
	    AM_PASSWORD: 'devpass',
	    AM_HOST: 'https://aggregatorquote-'+env+'.application.amfam.com',
	    AM_AUTHENTICATE_PATH: '/aggregatorquote/service/personalauto/v1/quote',
	 
	  };
	 
	  if(env == 'int') {
	    config.AM_USERNAME: 'agqsrvt'
	    config.AM_PASSWORD: 'd5FRm84k'
	  }
	 	 
	  karate.log('OpenAM Host:', config.AM_HOST);
	 
	  karate.configure('connectTimeout', 60000);
	  karate.configure('readTimeout', 60000);
	 
	  return config;
	}*/
/*function fn() {    
  var env = karate.env; // get system property 'karate.env'
 karate.configure('report', { showLog: true, showAllSteps: false })
  //Configure all response to be formatted in Pretty Print
  karate.configure('logPrettyResponse', true)
  
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
	ENDPOINT: 'https://aggregatorquote-int.application.amfam.com',
	DEFAULT_PATH: '/aggregatorquote/service/personalauto/v1/quote',
    myVarName: 'someValue',
  }
  if (env == int){
	  config.ENDPOINT: 'https://aggregatorquote-int.application.amfam.com';
	  config.DEFAULT_PATH: '/aggregatorquote/service/personalauto/v1/quote';
  }
  return config;
}*/