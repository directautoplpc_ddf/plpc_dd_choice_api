@API5
Feature: Get Submission for one vehicle and one driver 

Background: 


  * def data = read('classpath:resources/common/TestData/1V1D/Common_TestData.csv')  

  * def dataset = get data[?(@.Execute == 'Y')] 

Scenario Outline: <TestScenario>

   And if (Execute =='Y' && ContractState =='GA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_GA_CO.feature');}
   And if (Execute =='Y' && ContractState =='CO') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_GA_CO.feature');}
  
   And if (Execute =='Y' && ContractState =='TX') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_TX.feature');} 
   And if (Execute =='Y' && ContractState =='OR') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_OR_UT_MN_NV.feature');} 
   And if (Execute =='Y' && ContractState =='UT') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_OR_UT_MN_NV.feature');} 
   And if (Execute =='Y' && ContractState =='OH') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_OH_WI.feature');} 
   And if (Execute =='Y' && ContractState =='WI') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_OH_WI.feature');} 
   And if (Execute =='Y' && ContractState =='PA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_PA.feature');}
   And if (Execute =='Y' && ContractState =='MN') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_OR_UT_MN_NV.feature');} 
   And if (Execute =='Y' && ContractState =='IN') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_IN.feature');}
   And if (Execute =='Y' && ContractState =='NV') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_OR_UT_MN_NV.feature');} 
   And if (Execute =='Y' && ContractState =='MO') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_MO.feature');}
   And if (Execute =='Y' && ContractState =='MA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_MA.feature');}	
   And if (Execute =='Y' && ContractState =='KY') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_KY.feature');} 
   And if (Execute =='Y' && ContractState =='NM') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_NM.feature');}
   And if (Execute =='Y' && ContractState =='ME') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='WY') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='RI') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_RI.feature');}
   And if (Execute =='Y' && ContractState =='NJ') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_NJ.feature');}  
   And if (Execute =='Y' && ContractState =='MD') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_MD.feature');}
   And if (Execute =='Y' && ContractState =='NH') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_NH_MT.feature');}
   And if (Execute =='Y' && ContractState =='CT') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_CT.feature');} 
   And if (Execute =='Y' && ContractState =='NC') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_NC.feature');}
   And if (Execute =='Y' && ContractState =='AL') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='NE') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='IA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='NY') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_NY.feature');}
   And if (Execute =='Y' && ContractState =='DC') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_DC.feature');} 
   And if (Execute =='Y' && ContractState =='WA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_WA.feature');}
   And if (Execute =='Y' && ContractState =='MI') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_MI.feature');}  
   And if (Execute =='Y' && ContractState =='SD') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='MS') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_MS.feature');}
   And if (Execute =='Y' && ContractState =='VT') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_VT_VA_AK.feature');} 
   And if (Execute =='Y' && ContractState =='VA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_VT_VA_AK.feature');} 
   And if (Execute =='Y' && ContractState =='LA') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_LA_OK.feature');} 
   And if (Execute =='Y' && ContractState =='WV') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_WV.feature');}
   And if (Execute =='Y' && ContractState =='DE') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_DE.feature');}
   And if (Execute =='Y' && ContractState =='KS') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_KS.feature');}  
   And if (Execute =='Y' && ContractState =='MT') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_NH_MT.feature');}
   And if (Execute =='Y' && ContractState =='ID') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='SC') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_SC.feature');}
   And if (Execute =='Y' && ContractState =='AR') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_AR.feature');} 
   And if (Execute =='Y' && ContractState =='AK') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_VT_VA_AK.feature');} 
   And if (Execute =='Y' && ContractState =='FL') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_FL.feature');} 
   And if (Execute =='Y' && ContractState =='TN') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_TN.feature');}
   And if (Execute =='Y' && ContractState =='OK') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_LA_OK.feature');}  
   And if (Execute =='Y' && ContractState =='HI') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_HI.feature');} 
   And if (Execute =='Y' && ContractState =='AZ') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
   And if (Execute =='Y' && ContractState =='IL') {karate.call('classpath:resources/dynamic/StateRollout/1V1D/WAVE_1V1D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature');} 
  
  

  Examples: 
 | dataset | 
 
 
 
 
 
 
 
 
 
 
 
