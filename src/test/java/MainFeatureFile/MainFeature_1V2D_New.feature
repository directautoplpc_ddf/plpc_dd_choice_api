@API6
Feature: Get Submission for one vehicle and one driver 

Background: 


  * def data = read('classpath:resources/common/TestData/1V2D/Common_TestData.csv')  

  * def dataset = get data[?(@.Execute == 'Y')]  
  

Scenario Outline: <TestScenario>

And if (Execute =='Y' && ContractState =='GA') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_GA_CO.feature');}     
And if (Execute =='Y' && ContractState =='CO') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_GA_CO.feature');}
And if (Execute =='Y' && ContractState =='OR') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OR_UT_MN.feature');}
And if (Execute =='Y' && ContractState =='TX') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_TX.feature');}
And if (Execute =='Y' && ContractState =='UT') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OR_UT_MN.feature');} 
And if (Execute =='Y' && ContractState =='OH') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OH_WI.feature');}  
And if (Execute =='Y' && ContractState =='WI') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OH_WI.feature');} 
And if (Execute =='Y' && ContractState =='PA') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_PA.feature');}
And if (Execute =='Y' && ContractState =='MN') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OR_UT_MN.feature');}  
And if (Execute =='Y' && ContractState =='DE') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_DE.feature');} 
And if (Execute =='Y' && ContractState =='KS') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_KS.feature');}  
And if (Execute =='Y' && ContractState =='MT') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_MT.feature');} 
And if (Execute =='Y' && ContractState =='ID') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_ID.feature');} 
And if (Execute =='Y' && ContractState =='SC') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_SC.feature');} 
And if (Execute =='Y' && ContractState =='AR') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_AR.feature');} 
And if (Execute =='Y' && ContractState =='AK') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_AK.feature');} 
And if (Execute =='Y' && ContractState =='FL') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_FL.feature');} 
And if (Execute =='Y' && ContractState =='TN') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_TN.feature');} 
And if (Execute =='Y' && ContractState =='OK') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OK.feature');} 
And if (Execute =='Y' && ContractState =='HI') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_HI.feature');} 
And if (Execute =='Y' && ContractState =='AZ') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_AZ.feature');} 
And if (Execute =='Y' && ContractState =='IL') {karate.call('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_IL.feature');} 
       


  Examples: 
 | dataset | 
 

       

       
   