
@API2
Feature: Get Submission for Two vehicles and two drivers

Background: 
       
#2V2D-Operators      

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_GA_CO.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_GA_CO.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_TX.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_TX.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_OR_UT_MN_NV.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_OR_UT_MN_NV.csv') |       

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_OH_WI.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_OH_WI.csv') |     

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_PA.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_PA.csv') |  
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_IN.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_IN.csv') |  

	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_MO.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_MO.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_MA.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_MA.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_ME_WY_AL_NE_IA_SD_ID_AZ_IL.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_NH_MT.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_NH_MT.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_MD.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_MD.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_NJ.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_NJ.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_CT.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_CT.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_NM.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_NM.csv') |  	   
 
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_RI.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_RI.csv') |  
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_KY.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_KY.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_NC.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_NC.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_NY.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_NY.csv') |

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_DC.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_DC.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_MI.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_MI.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_WA.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_WA.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_LA_OK.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_LA_OK.csv') |  



Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_VT_VA_AK.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_VT_VA_AK.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_MS.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_MS.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_WV.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_WV.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_KS.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_KS.csv') | 


Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_DE.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_DE.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_SC.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_SC.csv') |  
 
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_AR.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_AR.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_TN.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_TN.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_FL.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_FL.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_Operators/WAVE_2V2D_HI.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_Operators/2V2D_Test Data_HI.csv') |  
	   
#SNI
 
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_GA_CO.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_GA_CO.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_TX.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_TX.csv') |   
            

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_OR_UT_MN_NV.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_OR_UT_MN_NV.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_OH_WI.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_OH_WI.csv') |   
              
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_PA.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_PA.csv') |  
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_IN.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_IN.csv') |    

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_MO.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_MO.csv') | 
	   
 
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_MA.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_MA.csv') | 
	   
 	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_ME_WY_AL_NE_IA_SD_ID_AZ_IL.feature')
         
       
Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_ME_WY_AL_NE_IA_SD_ID_AZ_IL.csv') |  
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_NH_MT.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_NH_MT.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_MD.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_MD.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_NJ.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_NJ.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_CT.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_CT.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_NM.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_NM.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_RI.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_RI.csv') | 
	   
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_KY.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_KY.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_NC.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_NC.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_NY.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_NY.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_DC.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_DC.csv') |
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_MI.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_MI.csv') | 
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_WA.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_WA.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_LA_OK.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_LA_OK.csv') |
	   

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_VT_VA_AK.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_VT_VA_AK.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_MS.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_MS.csv') |	

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_WV.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_WV.csv') |	   

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_KS.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_KS.csv') |	   

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_DE.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_DE.csv') |  
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_SC.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_SC.csv') |  
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_AR.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_AR.csv') | 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_FL.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_FL.csv') |  

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_HI.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_HI.csv') |  	 

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/2V2D_SNI/WAVE_2V2D_SNI_TN.feature')
         

Examples: 
       | read('classpath:resources/common/TestData/2V2D_SNI/2V2D_Test Data_SNI_TN.csv') |  	   
