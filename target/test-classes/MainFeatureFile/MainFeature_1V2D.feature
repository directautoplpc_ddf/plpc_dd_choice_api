@API4
Feature: Get Submission for one vehicle and two drivers 

Background: 
             
       
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_TX.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_TX.csv') |

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_GA_CO.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_GA_CO.csv') |
       
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OR_UT_MN.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_OR_UT_MN.csv') |

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OH_WI.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_OH_WI.csv') |
                   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_PA.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_PA.csv') |
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_MT.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_MT.csv') |
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_ID.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_ID.csv') |
       
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_DE.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_DE.csv') |   

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_KS.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_KS.csv') |
       

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_SC.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_SC.csv') |

Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_AR.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_AR.csv') |
       
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_AK.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_AK.csv') |
       
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_OK.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_OK.csv') |
       
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_TN.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_TN.csv') |
            
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_AZ.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_AZ.csv') |
	   
Scenario Outline: <TestScenario>

 And call read('classpath:resources/dynamic/StateRollout/1V2D/WAVE_1V2D_IL.feature')
       
Examples: 
       | read('classpath:resources/common/TestData/1V2D/1V2D_Test Data_IL.csv') |	   