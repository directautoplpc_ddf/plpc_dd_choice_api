@new
Feature: One Driver and One Vehicle

Background: Testing 
	* configure headers = {Content-Type: 'application/xml'}
	* configure ssl = true
	* configure ssl = 'TLSv1.2'
	
	* def writerfirst = Java.type('Utility.NameFaker') 
	* def firstname = writerfirst.methodname( ) 
	* def writerlast = Java.type('Utility.NameFaker') 
	* def lastname = writerlast.methodnametwo( ) 
	

# 	INT01
    * url 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11' 

# 	CTP QA
#   * url 'https://plpc-qa.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11' 
 
#   CTP INT
#   * url 'http://igwplco001:8080/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
 
# 	PINTD
#   * url 'http://dgwplco201:8080/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11' 

Scenario: Passing values from testdata 
	
	* xml req = read('classpath:resources/common/BaseXml/1V2D/State Rollout_1V2D_MT.xml')
	* def scenario = TestScenario

	
	* def LOBB = LOB == 'Null' ? karate.remove ('req','//LOB') : karate.set ('req','//LOB',LOB)
	* def contract = ContractState == 'Null' ? karate.remove ('req','//ContractState') : karate.set ('req','//ContractState',ContractState)
	
	#Line coverages
	
	* def coverage1 = PABodilyInjuryCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[1]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[1]/Terms/Entry/SelectedValue',PABodilyInjuryCov_af)
	* def coverage2 = PAPropDamageCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[2]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[2]/Terms/Entry/SelectedValue',PAPropDamageCov_af)
	* def coverage3 = PAMedExpCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[3]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[3]/Terms/Entry/SelectedValue',PAMedExpCov_af)
	* def coverage4 = PAUMCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[4]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[4]/Terms/Entry/SelectedValue',PAUMCov_af)
  * def coverage5 = PAUIMCov_af == 'Null' ? karate.remove ('req','//Product/Coverages/Entry[5]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/Coverages/Entry[5]/Terms/Entry/SelectedValue',PAUIMCov_af)
  
  #Effective date
   
   * def functions = call read('classpath:Utility/Utility.js')
	 * def EffectiveDate = functions.getCurrentDate( )
   * def date = EffectiveDate == 'Null' ? karate.remove ('req','//Product/EffectiveDate') : karate.set ('req','//Product/EffectiveDate',EffectiveDate+'T01:00:00-05:00')
    
  #PolicyAddress
    
    * def city = City == 'Null' ? karate.remove ('req','//Product/PolicyAddress/City') : karate.set ('req','//Product/PolicyAddress/City',City)
    * def country = Country == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Country') : karate.set ('req','//Product/PolicyAddress/Country',Country)
    * def Lat = Latitude == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Latitude') : karate.set ('req','//Product/PolicyAddress/Latitude',Latitude)
    * def Line = Line1 == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Line1') : karate.set ('req','//Product/PolicyAddress/Line1',Line1)
    * def Long = Longitude == 'Null' ? karate.remove ('req','//Product/PolicyAddress/Longitude') : karate.set ('req','//Product/PolicyAddress/Longitude',Longitude)
    * def state = State == 'Null' ? karate.remove ('req','//Product/PolicyAddress/State') : karate.set ('req','//Product/PolicyAddress/State',State)
    * def Zip = ZipCode == 'Null' ? karate.remove ('req','//Product/PolicyAddress/ZipCode') : karate.set ('req','//Product/PolicyAddress/ZipCode',ZipCode)
    
  #Affiliation
    
    * def Tier = MemberTier == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Affiliations/Entry/MemberTier') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Affiliations/Entry/MemberTier',MemberTier)
   
  #PNI Address
    
   * def PNICityy = PNICity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/City',PNICity)
   * def PNILine11 = PNILine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/Line1',PNILine1)
  #* def PNIShortCityy = PNIShortCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ShortCity') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ShortCity',PNIShortCity)
   * def PNIStatee = PNIState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/State',PNIState)
   * def PNIZipcodee = PNIZipcode == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ZipCode',PNIZipcode)
 
  #PNI Details
    
    * def birthDate = BirthDate == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/BirthDate') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/BirthDate',BirthDate)
    * def firstName = FirstName == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/FirstName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/FirstName',firstname)
    * def lastName = LastName == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LastName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LastName',lastname)
    * def License = LicenseNumber == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseNumber') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseNumber',LicenseNumber)
    * def licenseState = LicenseState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseState') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/LicenseState',LicenseState)
   
    * def RReferenceId = ReferenceId == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry/ReferenceId') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry/ReferenceId',ReferenceId)
  
  #second driver
    
   * def OperatorCityy = OperatorCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/City',OperatorCity)
   * def OperatorLine11 = OperatorLine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/Line1',OperatorLine1)
  #* def PNIShortCityy = PNIShortCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ShortCity') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[1]/Person/Addresses/Entry/ShortCity',PNIShortCity)
   * def OperatorStatee = OperatorState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/State',OperatorState)
   * def OperatorZipcodee = OperatorZipcode == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/Addresses/Entry/ZipCode',OperatorZipcode)
 
  #second driver Details
    
    * def OperatorbirthDatee = OperatorBirthDate == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/BirthDate') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/BirthDate',OperatorBirthDate)
    * def OperatorfirstNamee = OperatorFirstName == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/FirstName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/FirstName',OperatorFirstName)
    * def OperatorlastNamee = OperatorLastName == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LastName') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LastName',OperatorLastName)
    * def OperatorLicensee = OperatorLicenseNumber == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseNumber') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseNumber',OperatorLicenseNumber)
    * def OperatorlicenseStatee = OperatorLicenseState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseState') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/Person/LicenseState',OperatorLicenseState)
   
    * def RReferenceId2 = ReferenceId2 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/ReferenceId') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/PolicyDrivers/Entry[2]/ReferenceId',ReferenceId2)  
 
  #Vehicle1 coverage 
   
    * def Vehicle1PACollisionCov = PACollisionCov == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Coverages/Entry[1]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Coverages/Entry[1]/Terms/Entry/SelectedValue',PACollisionCov)
    * def Vehicle1PAComprehensiveCov = PAComprehensiveCov == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Coverages/Entry[2]/Terms/Entry/SelectedValue') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Coverages/Entry[2]/Terms/Entry/SelectedValue',PAComprehensiveCov)
  
  
 #Vehicle driver assignment:
  * def CClassRatedDriver = ClassRatedDriver == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry/ClassRatedDriver') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry/ClassRatedDriver',ClassRatedDriver)
  * def DDriverType = DriverType == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry/DriverType') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry/DriverType',DriverType)
  * def PPolicyDriverRefId = PolicyDriverRefId == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry/PolicyDriverRefId') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry/PolicyDriverRefId',PolicyDriverRefId) 
   
  * def CClassRatedDriver2 = ClassRatedDriver2 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry[2]/ClassRatedDriver') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry[2]/ClassRatedDriver',ClassRatedDriver2)
  * def DDriverType2 = DriverType2 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry[2]/DriverType') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry[2]/DriverType',DriverType2)
  * def PPolicyDriverRefId2 = PolicyDriverRefId2 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry[2]/PolicyDriverRefId') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry/Drivers/Entry[2]/PolicyDriverRefId',PolicyDriverRefId2) 
   
 #Vehicle1 Garaged address
   
   * def Veh1GaragedCity = Vehicle1GaragedCity == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/City') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/City',Vehicle1GaragedCity)
   * def Veh1GaragedCounty = Vehicle1GaragedCounty == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/County') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/County',Vehicle1GaragedCounty)
   * def Veh1GaragedLatitude = Vehicle1GaragedLatitude == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Latitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Latitude',Vehicle1GaragedLatitude)
   * def Veh1GaragedLine1 = Vehicle1GaragedLine1 == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Line1') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Line1',Vehicle1GaragedLine1)
   * def Veh1GaragedLongitude = Vehicle1GaragedLongitude == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Longitude') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/Longitude',Vehicle1GaragedLongitude)
   * def Veh1GaragedState = Vehicle1GaragedState == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/State') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/State',Vehicle1GaragedState)
   * def Veh1GaragedZipCode = Vehicle1GaragedZipCode == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ZipCode') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/GaragedLocation/LocationAddress/ZipCode',Vehicle1GaragedZipCode)
 
 #Vehicle1 details  
      
    * def Vehicle1Model = Model == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Model') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Model',Model)
    * def Vehicle1Vin = Vin == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Vin') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Vin',Vin)
    * def Vehicle1Year = Year == 'Null' ? karate.remove ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Year') : karate.set ('req','//Product/PolicyLineType/PersonalAutoPolicy/Vehicles/Entry[1]/Year',Year)
  
 #PrefillID
   
    * def PrefillID = PrefillReportID == 'Null' ? karate.remove ('req','//Product/PrefillReportID') : karate.set ('req','//Product/PrefillReportID',PrefillReportID)
    
 #Primary name insured

    * def PNICityy = PNICity == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/City') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/City',PNICity)
    * def PNILine11 = PNILine1 == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Line1') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/Line1',PNILine1)
   # * def PNIShortCityy = PNIShortCity == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ShortCity') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ShortCity',PNIShortCity)
    * def PNIStatee = PNIState == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/State') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/State',PNIState)
    * def PNIZipcodee = PNIZipcode == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ZipCode') : karate.set ('req','//Product/PrimaryNamedInsured/Person/Addresses/Entry/ZipCode',PNIZipcode)
    * def birthDate = BirthDate == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/BirthDate') : karate.set ('req','//Product/PrimaryNamedInsured/Person/BirthDate',BirthDate)
    * def firstName = FirstName == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/FirstName') : karate.set ('req','//Product/PrimaryNamedInsured/Person/FirstName',firstname)
    * def lastName = LastName == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/LastName') : karate.set ('req','//Product/PrimaryNamedInsured/Person/LastName',lastname)
    * def License = LicenseNumber == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/LicenseNumber') : karate.set ('req','//Product/PrimaryNamedInsured/Person/LicenseNumber',LicenseNumber)
    * def licenseState = LicenseState == 'Null' ? karate.remove ('req','//Product/PrimaryNamedInsured/Person/LicenseState') : karate.set ('req','//Product/PrimaryNamedInsured/Person/LicenseState',LicenseState)
   
  #PriorPolicies
    
    * def PriorPolicyFirstname = PolicyFirstName == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/PolicyFirstName') : karate.set ('req','//Product/PriorPolicies/Entry/PolicyFirstName',firstname)
    * def PriorPolicyFullName = PolicyLastName == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/PolicyFullName') : karate.set ('req','//Product/PriorPolicies/Entry/PolicyFullName',firstname+lastname)
    * def PriorPolicyLastName = PolicyLastName == 'Null' ? karate.remove ('req','//Product/PriorPolicies/Entry/PolicyLastName') : karate.set ('req','//Product/PriorPolicies/Entry/PolicyLastName',lastname)
    
    Given request req 
    When method POST 
    And print response 
    And print scenario 
  
	#Checking whether getting submission id in GB Response
	
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200
	
	# output response
	And def submission = karate.jsonPath(response,"$..['pogo:SubmissionID']")[0]
	And print submission 
	And match submission != null 
  
  #Exporting submission id
	And string sub = submission
	And def demowrite = Java.type('Utility.WriteCode') 
	And def Addres = demowrite.writetofile(sub,scenario)

 #Premium Validation
	And def MonthlyPremium = karate.jsonPath(response,"$..['pogo:MonthlyPremium']")[0]
	And print 'MonthlyPremium', MonthlyPremium    
	And match MonthlyPremium != null
	And def TotalPremium = karate.jsonPath(response,"$..['pogo:TotalPremium']")[0]
	And print 'TotalPremium', TotalPremium
	And match TotalPremium != null
	
	# GetBindable Premium Verify with Recalc
	And xml requ = read ('classpath:resources/common/BaseXml/RecalcPremium.xml') 
	And karate.remove ('requ','/soapenv:Envelope/soapenv:Body/pcs:recalc/pcs:aRequest/ent:SubmissionID') 
	And karate.set ('requ','/soapenv:Envelope/soapenv:Body/pcs:recalc/pcs:aRequest/ent:SubmissionID',submission) 
	And print requ 
	Given request requ 
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
	Then retry until responseStatus == 200
	And print 'response: ', response 
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	# Then assert responseStatus == 200
	#output response
	And def submission_GetBindableRecalc = karate.jsonPath(response,"$..['pogo:SubmissionID']")[0] 
	And print submission_GetBindableRecalc
	And match submission_GetBindableRecalc != null 
  #Exporting submission id
	And string sub = submission_GetBindableRecalc 
	And def demowrite = Java.type('Utility.WriteCode') 
	And def Addres = demowrite.writetofile(sub,scenario)
	
 	#RecalcMemberAdd
			  
  And xml reqsn = read ('classpath:resources/common/BaseXml/RecalcMember.xml') 
  And karate.remove ('reqsn','/soap:Envelope/soap:Body/ns2:recalc/ns2:aRequest/SubmissionID') 
  And print reqsn
  And karate.set ('reqsn','/soap:Envelope/soap:Body/ns2:recalc/ns2:aRequest/SubmissionID',submission)
  And print reqsn
	Given request reqsn 
	 
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	Then retry until responseStatus == 200
	And print 'response: ', response 
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200
	
  # GetForms_to be signed Document
	  
  And xml reqsn = read ('classpath:resources/common/BaseXml/getSignatureForms.xml') 
  And karate.remove ('reqsn','/soapenv:Envelope/soapenv:Body/pcs:getSignatureForms/pcs:signDocumentRequestType/ent:SubmissionID') 
  And print reqsn
  And karate.set ('reqsn','/soapenv:Envelope/soapenv:Body/pcs:getSignatureForms/pcs:signDocumentRequestType/ent:SubmissionID',submission)
  And print reqsn
  Given request reqsn 
	 
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	Then status 200 
  And print 'response: ', response 
  # check if the response status is 200
	And def uploadStatusCode = responseStatus 
 Then assert responseStatus == 200
	 
  # Getdocsigned Document
	  
  And xml reqdcn = read ('classpath:resources/common/BaseXml/getdocforonlinesign.xml') 
  And karate.remove ('reqdcn','/soapenv:Envelope/soapenv:Body/pcs:getDocumentsForOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID') 
  And print reqdcn
  And karate.set ('reqdcn','/soapenv:Envelope/soapenv:Body/pcs:getDocumentsForOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID',submission)
  And print reqdcn
	Given request reqdcn 
	
	#Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	 Then status 200 
   And print 'response: ', response 
	 # check if the response status is 200
	 And def uploadStatusCode = responseStatus 
	 Then assert responseStatus == 200
	 
	 # Signed Document online
	And xml reqso = read ('classpath:resources/common/BaseXml/signDocUOnline.xml') 
	And karate.remove ('reqso','/soapenv:Envelope/soapenv:Body/pcs:signDocumentsUsingOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID') 
	And karate.set ('reqso','/soapenv:Envelope/soapenv:Body/pcs:signDocumentsUsingOnlineSign/pcs:signDocumentRequestType/ent:SubmissionID',submission) 
	And print reqso 
	Given request reqso 

  #Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
      
	
	Then status 200 
	And print 'response: ', response 
	# check if the response status is 200
	And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200 
	 
	Then status 200 
	And print 'response: ', response 
	# check if the response status is 200
  And def uploadStatusCode = responseStatus 
	Then assert responseStatus == 200
	  
 # Bind submission
  And xml reqs = read ('classpath:resources/common/BaseXml/submission.xml') 
  And karate.remove ('reqs','/soapenv:Envelope/soapenv:Body/pcs:bindSubmission/pcs:submissionID') 
  And karate.set ('reqs','/soapenv:Envelope/soapenv:Body/pcs:bindSubmission/pcs:submissionID',submission) 
  And print reqs 
  Given request reqs 
  #Environment
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionWorkflowService/soap11'
              
   Then status 200 
   Then retry until responseStatus == 200
   And print 'response: ', response 
  # check if the response status is 200
   And def uploadStatusCode = responseStatus 
   Then assert responseStatus == 200
   
  # Retrieve submission
              * configure headers = {Content-Type: 'text/xml; charset=utf-8'}
              * configure logPrettyResponse = true
              * configure ssl = true
              * configure ssl = 'TLSv1.2'
              * configure readTimeout = 3000000

              * configure charset = null
              * configure headers = {Cookie : null}
              
    #Environment
              
    * url 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionDataService/soap11'
              
  
  And xml reqs = read ('classpath:resources/common/BaseXml/RetrieveSubmission.xml') 
  And karate.remove ('reqs','/soapenv:Envelope/soapenv:Body/pcs:retrieveSubmission/pcs:aRequest/ent:SubmissionID') 
  And karate.set ('reqs','/soapenv:Envelope/soapenv:Body/pcs:retrieveSubmission/pcs:aRequest/ent:SubmissionID',submission) 
  And print reqs 

  Given request reqs 
  When soap action 'https://plpc-int01.amfam.com/pc/ws/amfam/webservice/portal/endpoint/PCSubmissionDataService/soap11'
  Then status 200
 # Then retry until status == 200
  And print 'response: ', response
  
 #Validation in Policy retrive
 
  #1. Validating Bodily injury
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag1
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD1
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
  #2. Validating Medical expense     
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag2
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD2
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
   #3. Validating Property damage      
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag3
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD3
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
    #4. Validating Comprehensive     
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag4
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD4
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
    #5. Validating Collision     
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag5
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:SelectedValue']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD5
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
    
    #6. Validating Accident Free discount
    And def submission1 = karate.jsonPath(response,"$..['pogo:SubmissionID']")
    And print 'Submission id from policy retrive', submission1
    And def respt = $
    And def dyn = PolicysearchTag6
    And json actualValue = karate.jsonPath(respt, "$[*]..[?(@.pogo:Code=='" + dyn + "')]")
    And print 'Response from Policy retrive', actualValue
    And def explValue = karate.jsonPath(actualValue,"$..['pogo:Selected']")
    And print 'Response value for the coverage', explValue
    And json ex = Policyexpval_VD6
    And def actual = karate.toJson(explValue)
    And def expected = karate.toJson(ex)
    Then match actual == expected
